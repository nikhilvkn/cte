log_level                :info
log_location             STDOUT
node_name                'admin'
client_key               '/home/vagrant/chef-repo/.chef/admin.pem'
validation_client_name   'adminorganization-validator'
validation_key           '/home/vagrant/chef-repo/.chef/adminorganization-validator.pem'
chef_server_url          'https://server.example.tld/organizations/adminorganization'
syntax_check_cache_path  '/home/vagrant/chef-repo/.chef/syntax_check_cache'
cookbook_path [ '/home/vagrant/chef-repo/cookbooks' ]
