#!/bin/bash

if [ `dpkg -l | grep -i "chefdk" | wc -l` = 0 ]; then
echo "Installing CDK Package" 
    if [ `(ls /vagrant/) | grep -i ".deb" | wc -l` = 0 ]; then
	wget -P /vagrant/ https://packages.chef.io/stable/ubuntu/12.04/chefdk_0.16.28-1_amd64.deb
	else
	echo "Package Found in /vagrant/"
	fi
    #find /vagrant/ -type f -name "*.deb"
    FILE=`(ls /vagrant/) | grep -i "chefdk*"`
	#Install git,shpass and configure git here	
    sudo dpkg -i /vagrant/$FILE
	sudo apt-get install git -y
	#Configure GIT
	git config --global user.name admin
    git config --global user.email admin@example.tld
	#Verify Installation
	chef verify
		if [ $? -eq 0 ]; then
        echo "Chef DK is Installed Sucessfully"
        fi
	echo "CDK Configuration Begins"		
	echo "Generate the chef repo"
	if [ ! -d chef-repo/ ]; then
		#sudo apt-get install sshpass git -y
		chef generate repo chef-repo
		mkdir chef-repo/.chef
		cp /vagrant/keys/* chef-repo/.chef/
		#sshpass -p "vagrant" scp -r vagrant@server.example.tld:/vagrant/keys/*.pem /vagrant/chef-repo/.chef/
		#echo "Adding Version Control"
		cp /vagrant/knife.rb chef-repo/.chef/
		sudo /bin/chown -R vagrant.vagrant chef-repo/
		cd chef-repo/
		/usr/bin/knife ssl fetch
		/usr/bin/knife client list
	fi
		if [ $? -eq 0 ]; then
        echo "Configuration Completed Sucessfully"
        fi
	echo "Bootstrapping A Node"
	/usr/bin/knife bootstrap node.example.tld -N node_1 -x vagrant -P vagrant --sudo --use-sudo-password -N Node1
	/usr/bin/knife node list
else
echo "CDK is installed"
fi