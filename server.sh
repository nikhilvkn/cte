#!/bin/bash
#cd "$(/home/vagrant "$0")"
if [ `dpkg -l | grep -i "chef" | wc -l` = 0 ]; then
echo "Installing Chef Package" 
    if [ `(ls /vagrant/) | grep -i ".deb" | wc -l` = 0 ]; then
	wget -P /vagrant/ https://packages.chef.io/stable/ubuntu/14.04/chef-server-core_12.8.0-1_amd64.deb
	else 
	echo "Server Package Found In /vagrant"
	fi
    #find /vagrant/ -type f -name "*.deb"
    FILE=`(ls /vagrant/) | grep -i "chef-server*"`
    sudo dpkg -i /vagrant/$FILE
    sudo chef-server-ctl reconfigure
    echo "Creating Chef User and Organization"
	if [ ! -d /vagrant/keys ]; then
		sudo mkdir .keys /vagrant/keys
		sudo chef-server-ctl user-create admin admin admin admin@example.tld 1qazXSW@ -f .keys/admin.pem
		sudo chef-server-ctl org-create adminorganization adminorganization --association_user admin -f .keys/adminorganization-validator.pem
		#Create directory in Shared Directory (/vagrant/keys)
		cp .keys/* /vagrant/keys/
		sudo /bin/chown -R vagrant.vagrant .keys/
        if [ $? -eq 0 ]; then
        echo "Chef Server Configuration Completed Sucessfully"
		A=`ifconfig eth1 | grep "inet " | awk -F'[: ]+' '{ print $4 }'`
		echo "IP Address is $A"
        fi
	fi
echo "Chef is installed"
fi
